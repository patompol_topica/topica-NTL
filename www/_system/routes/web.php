<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

//Auth
Route::get('/', function() {
    return view('home');
});
Route::post('auth/login', ['as' => 'auth_login', 'uses' => 'Auth\AuthController@login']);

//User
Route::get('user/profile/{id}', ['as' => 'login_profile', 'uses' => 'UserController@profile']);
Route::get('user/edit/{id}', []);
Route::get('user/add/{id}', []);
Route::get('user/delete/{id}', []);

//API
Route::post('api/user', 'UserController@json');

//3rd Party
Route::get('auth/google', 'Auth\AuthController@redirectToProvider');
Route::get('auth/google/callback', 'Auth\AuthController@handleProviderCallback');

//Others
Route::get('info', function() {
    return view('/info');
});

Route::get('layout', function() {
    return view('/layout');
});

Route::get('/home', 'HomeController@index')->name('home');