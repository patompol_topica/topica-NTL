<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RolePage extends Model
{
    
    const PAGE = 'main';
    const SUB_PAGE = 'sub';
    const DEFAULT_PAGE = 'default_page';
    
    protected $table = 'role_page';
    //protected  
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
    public static function getPage($role_id, $default_page_id) {
        
        $pages = [];
        
        $rawPages = self::where('role_id', '=', $role_id)
                ->join('page', 'role_page.page_id', '=', 'page.id')
                ->orderBy('root_id', 'asc')
                ->get();
        
        $array = [];
        
        foreach ($rawPages as $page) {
            if(empty($page->root_id)) {
                array_push($pages, $page);
                $page[self::SUB_PAGE] = self::getSubPage($rawPages, $page->id);
            } 
            
            if($page->id == $default_page_id) {
                $role_page[self::DEFAULT_PAGE] = $page->url;
            }
            
        }
        
        $role_page[self::PAGE] = $pages;
        
        return $role_page;
        
    }
    
    public static function getSubPage($rawPages, $root_id) {
        
        $array = [];
        
        foreach ($rawPages as $page) {
            if($page->root_id == $root_id){
                array_push($array, $page);
            }
        }
        
        return $array;
        
        
    }
    
}
