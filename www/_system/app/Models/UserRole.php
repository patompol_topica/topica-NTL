<?php

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    
    protected $table = 'user_role';
    //protected  

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
}
