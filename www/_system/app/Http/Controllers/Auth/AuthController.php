<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;

use App\Models\User;

use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller 
{
    
    public function login(LoginRequest $request) {
        
        $username = $request->username;
        $password = $request->password;
        
        $username = 'manager';
        $password = 'password';
        
        if (!Auth::attempt(['name' => $username, 'password' => $password]))
        {
            return response()->json([
                'status' => false, 
                'code' => 'invalid username or password'
                ]);     
            
        } 
        
        return redirect()->route('login_profile', ['id' => Auth::user()->id]);
        
        
    }
    
    public function redirectToProvider() 
    {
        return Socialite::with('google')->redirect();        
    }
    
    public function handleProviderCallBack() 
    {
        
        $user = Socialite::with('google')->user();
        
        if (strpos($user->getEmail(), '@topica.com')) {
            //register
            //$user->getId();
            //$token = $user->token;
            //$expiresIn = $user->expiresIn;
            //$user->getAvatar(); //picture            
        } else {
            //echo 'Invalid User';
        }
        
    }
    
    
     
}
 