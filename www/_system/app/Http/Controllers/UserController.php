<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserRole;
use App\Models\RolePage;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class UserController extends Controller {
    
    
    public function profile(Request $request) {
        
        $user = User::find($request->id)
                ->first();

        //dd($user);
        
        //return;
        
        if(!empty($user)) {

            //echo $user;

            $user_role = UserRole::where('user_id', '=', $user->id)
                    ->first();          
            $user->role_id = $user_role->role_id;          
            $role = DB::table('role')
                    ->select('default_page_id')
                    ->where('id', '=', $user_role->role_id)
                    ->first();
            $role_page_data = RolePage::getPage($user->role_id, $role->default_page_id);
            $user->page = $role_page_data[RolePage::PAGE];
            $user->default_page = $role_page_data[RolePage::DEFAULT_PAGE];

            echo $user;



        } else {
            echo 'no user';
        }

    }
    

  public function index() {
      
      //TODO add edit/delete btn
      
      $userList = User::all();
      //$users = App\User::where('id', '>=', 2)->get();
      //$userList = User::orderBy('id', 'DESC')->get();
      
      //return $users->toArray();
      
      return view('/user/list')->with('userList', $userList);
      
  }
  
  public function add($name) {
      
      $user = new User();
      $user->name = $name;
      $user->save();
      
      //echo 'register';
      
  }
  
  public function edit($id) {
      
      $user = User::find($id);
      
      if(empty($user)) {
          //TODO return to list with NOTI
          return "no user";      
      }
      
      return view('/user/edit')->with('user', $user);
      
  }
  
  public function remove($id) {
      
      $user = User::find($id);
      
      if(!empty($user)) {
          $user->deleted_at = date('Y-m-d H:i:s');
          $user->save();
          echo 'deleted';
      } else {          
          echo 'no user id: '.$id;
      }
      
  }
  
  public function json() {
      
      $userList = User::all();
      
      return $userList->toArray();;
      
  }
  
  
  
}

?>
