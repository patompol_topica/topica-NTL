<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
    <header class="row">
        @include('includes.header')
    </header>
   
    <div>
        <form action="{{ route('auth_login') }}" method="POST" >
            {{ csrf_field() }}
            <div>
                <label>Username</label>
                <input name="username" type="text"
                       placeholder="Enter ..."><br>
                <label>Password</label>
                <input name="password" type="password"
                       placeholder="Enter ..."><br>
                <button type="submit">Login</button><br><br>       
                
                <!--<input type="hidden" name="_token" value="{{ csrf_token() }}">-->
                
            </div>
            
        </form>
        
        @yield('content')
        
    </div>
    
    <footer class="row">
        @include('includes.footer')
    </footer>

</div>
</body>
</html>