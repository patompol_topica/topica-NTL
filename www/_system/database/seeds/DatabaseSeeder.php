<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->command->info('User table seeded');
    }
}

class UsersTableSeeder extends Seeder {
    
    public function run() 
    {
        //DB::table('user')->delete();
        
    }
    
}